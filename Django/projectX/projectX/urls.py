from django.conf.urls import patterns, include, url
from django.contrib.auth.models import User, Group, Permission
from rest_framework import viewsets, routers
from django.contrib import admin
from app import views
admin.autodiscover()


# ViewSets define the view behavior.
#class UserViewSet(viewsets.ModelViewSet):
#    model = User

class GroupViewSet(viewsets.ModelViewSet):
    model = Group

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'accounts', views.UserViewSet, 'list')
router.register(r'companies', views.CompanyViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'inquiries', views.InquiryViewSet)
router.register(r'offers', views.OfferViewSet)
#router.register(r'permissions', views.CompanyViewSet)




urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'projectX.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include(router.urls)),
    url(r'^', include('app.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
)
