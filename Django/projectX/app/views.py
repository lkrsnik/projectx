from django.shortcuts import render
from django.core.context_processors import csrf
from datetime import datetime
# Create your views here.

#from rest_framework import status
#from rest_framework.views import APIView
#from rest_framework.response import Response
from django.contrib.auth.models import User, Group

from rest_framework import viewsets, permissions, filters, status, generics

#LOG-IN
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.authtoken.models import Token
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
import json
#------
#PAGINATION
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.pagination import PaginationSerializer
#------

from rest_framework.decorators import link,action
from rest_framework.response import Response
from rest_framework.reverse import reverse

from app.models import Company, User_Company, User_FloatingData, Inquiry, Offer
from app.serializers import CompanySerializer, UserSerializer, AuthenticateSerializer, GetMeSerializer, GroupSerializer, User_CompanySerializer
from app.serializers import PaginatedCompanySerializer, User_FloatingDataSerializer, UserModifySerializer, InquirySerializer, ChangePasswordSerializer
from app.serializers import PaginatedUsersSerializer, CompanyApprovementSerializer, PaginatedInquirySerializer, OfferSerializer
from app.permissions import IsOwnerOrReadOnly, IsStaffOrTargetUser


class CompanyViewSet(viewsets.ModelViewSet):
    #`list`, `create`, `retrieve`, `update` and `destroy`
    queryset = Company.objects.all()
    
    serializer_class = CompanySerializer
    def get_permissions(self):
        # allow non-authenticated user to create via POST
        return (permissions.AllowAny() if self.request.method == 'POST' or self.request.method == 'GET' or self.request.method == 'PUT'
            else IsStaffOrTargetUser()),
    # def pre_save(self, obj):
    #     obj.companyCreator = self.request.user

    #def retrieve(self, request, pk=None):
   # 	print "im GETing!"
   #     pass

    def list(self, request, pk=None):
        #print User.objects.get(id = 3)
        #print self.request.user
        print request.user
        #print Company.objects.filter(name__contains="test")
        reverseSearch = request.QUERY_PARAMS.get('reverseSearch')
        name = request.QUERY_PARAMS.get('name')
        address = request.QUERY_PARAMS.get('address')
        workArea = request.QUERY_PARAMS.get('workArea')
        orderBy = request.QUERY_PARAMS.get('orderBy')
        if name == None:
            name=""

        if address == None:
            address=""

        if workArea == None:
            workArea=""

        if not (orderBy == 'name' or orderBy == 'address' or orderBy == 'workArea' or orderBy == 'is_confirmed'):
            orderBy = 'name'

        if reverseSearch == 'true':
            orderBy = "-"+orderBy

        print 
        if request.user.groups.all()[0].name == 'editor' or request.user.groups.all()[0].name == 'admin':
    		queryset = Company.objects.filter(name__icontains=name, address__icontains=address, workArea__icontains=workArea).order_by(orderBy)
        else:
        	queryset = Company.objects.filter(is_confirmed=True, name__icontains=name, address__icontains=address, workArea__icontains=workArea).order_by(orderBy)
        paginator = Paginator(queryset, 10)


        page = request.QUERY_PARAMS.get('page')
        try:
            companies = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            companies = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999),
            # deliver last page of results.
            companies = paginator.page(paginator.num_pages)

        serializer_context = {'request': request}
        #serializer = self.get_serializer(queryset, many=True)

        serializer = PaginatedCompanySerializer(companies,
                                         context=serializer_context)

        return Response(serializer.data)

    def create(self, request):
        companyElements = request.DATA
        serializer = self.get_serializer(data=companyElements)
        if serializer.is_valid():
            serializer.object.companyCreator = self.request.user
            #serializer.object.companyCreator = User.objects.get(id = 3)
            # ADD TRANSACTION
            createdCompany = serializer.save()
            midSerializer = User_CompanySerializer(data={'is_confirmed': True, 'can_converse': True, 'can_accept_inquiry': True,
                       'can_apply_inquiry': True, 'can_post_inquiry': True, 'can_change_company_properties': True, 'can_grant_company_privileges': True})
            if midSerializer.is_valid():
                
            	midSerializer.object.user = self.request.user
                #midSerializer.object.user = User.objects.get(id = 3)

            	midSerializer.object.company = createdCompany
            	midSerializer.save()
                #print serializer.data
            	return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
            	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
    	print "HEREEE!!!"
        queryset = Company.objects.get(id=pk)
        user = self.request.user
        #user = User.objects.get(id = 1)
        #print user.groups.all()[0].name == 'regular'
        #print type(request.DATA['companyCreator'])
        if request.DATA['name'] != queryset.name and user.groups.all()[0].name == 'regular':
            return Response(status=status.HTTP_400_BAD_REQUEST)

        print request.DATA
        serializer = CompanySerializer(queryset, data=request.DATA)
        #print serializer.data
        if serializer.is_valid():
            #User.objects.get()
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        #print type(int(pk))
        queryset = Company.objects.all()
        try:
            nothing = int(pk)
            company = get_object_or_404(queryset, pk=pk)
        except:
            company = get_object_or_404(queryset, name=pk)
            pass
        
        #company = get_object_or_404(queryset, pk=pk)
        serializer = CompanySerializer(company)
        return Response(serializer.data)

class CompanyApprovementView(GenericAPIView):
    permission_classes = ((permissions.IsAuthenticated,))
    serializer_class = CompanyApprovementSerializer
    def get(self, request):
        queryset = Company.objects.filter(id=request.QUERY_PARAMS.get('id'))
        serializer = CompanyApprovementSerializer(queryset, many=True)
        return Response(serializer.data)
    def post(self, request):
        queryset = Company.objects.filter(id=request.DATA)
        if len(queryset)!=1:
            return Response({"details": "This ID doesn't exist."}, status=status.HTTP_400_BAD_REQUEST)
        print queryset
        serializer = CompanyApprovementSerializer(queryset[0], data={})
        print "HEREE22222!!!"


        if serializer.is_valid():
            print "HEREE333333!!!"
            serializer.object.is_confirmed = True
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class User_CompanyView(GenericAPIView):
    permission_classes = ((permissions.IsAuthenticated,))
    serializer_class = User_CompanySerializer

    def post(self, request):
        # Create a serializer with request.DATA
        user = request.user
        company = Company.objects.get(id = request.DATA)
        if len(User_Company.objects.filter(user=request.user,company=company))>0:
            return Response({"details": "You already applied for this company."}, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.serializer_class(data={})
        if serializer.is_valid():
            serializer.object.user = user
            serializer.object.company = company
            #serializer.object.user = self.request.user
            #serializer.object.user = User.objects.get(id = 3)
            #serializer.object.company = company
            serializer.save()
            #print serializer.data
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request):
        print request.DATA['company']
        #company = Company.objects.get()
        queryset = User_Company.objects.get(id=request.DATA['id'])
        serializer = User_CompanySerializer(queryset, data=request.DATA)
        #print serializer.data
        if serializer.is_valid():
            #User.objects.get()
            serializer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def get(self, request):
        companyID = request.QUERY_PARAMS.get('company')
        queryset = User_Company.objects.filter(company__id=companyID)
        serializer = User_CompanySerializer(queryset, many=True)
        return Response(serializer.data)

class UserViewSet(viewsets.ModelViewSet):
	#`list` and `detail`
	#queryset = User.objects.all()
    serializer_class = UserSerializer
    model = User
	#permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    def get_permissions(self):
        # allow non-authenticated user to create via POST
        return (permissions.AllowAny() if self.request.method == 'POST'
                else IsStaffOrTargetUser()),

    def list(self, request, pk=None):
        #print User.objects.get(id = 3)
        #print self.request.user
        
        #print Company.objects.filter(name__contains="test")
        reverseSearch = request.QUERY_PARAMS.get('reverseSearch')
        username = request.QUERY_PARAMS.get('username')
        first_name = request.QUERY_PARAMS.get('first_name')
        last_name = request.QUERY_PARAMS.get('last_name')
        email = request.QUERY_PARAMS.get('email')
        orderBy = request.QUERY_PARAMS.get('orderBy')
        #print reverseSearch
        if username == None:
            username=""

        if first_name == None:
            first_name=""

        if last_name == None:
            last_name=""

        if email == None:
            email=""

        if not (orderBy == 'username' or orderBy == 'first_name' or orderBy == 'last_name' or orderBy == 'email'):
            orderBy = 'username'

        #print reverseSearch
        if reverseSearch == 'true':
            orderBy = "-"+orderBy
            
        #print orderBy


        queryset = User.objects.filter(username__icontains=username, first_name__icontains=first_name, last_name__icontains=last_name, email__icontains=email).order_by(orderBy)
        paginator = Paginator(queryset, 10)


        page = request.QUERY_PARAMS.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            users = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999),
            # deliver last page of results.
            users = paginator.page(paginator.num_pages)

        serializer_context = {'request': request}
        #serializer = self.get_serializer(queryset, many=True)

        serializer = PaginatedUsersSerializer(users,
                                         context=serializer_context)

        return Response(serializer.data)


    def update(self, request, pk=None):
        queryset = User.objects.get(id=pk)
        group = Group.objects.get(id=request.DATA['groups'][0])

        #group.user_set.add(queryset)
        print group
        
        del request.DATA['groups']
        #print request.DATA
        serializer = UserModifySerializer(queryset, data=request.DATA)
        #print serializer.data
        if serializer.is_valid():

            #print serializer.object.groups
            #serializer.object.groups.all()[0].remove(queryset)
            queryset.groups.all()[0].user_set.remove(queryset)
            group.user_set.add(queryset)
            if group.name == 'admin':
                queryset.is_staff = True
            else:
                queryset.is_staff = False
            serializer.save()
            #print serializer.data
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request):
        serializer = self.get_serializer(data=request.DATA)
        if serializer.is_valid():
            g = Group.objects.get(name='regular') 
            user = serializer.save()
            g.user_set.add(user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AccountPassword(generics.GenericAPIView):
    """
    Change password of the current user.
    
    **Accepted parameters:**
    
     * current_password
     * password1
     * password2
    """
    #print "HERE"
    #authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ChangePasswordSerializer
    
    def post(self, request, format=None):
        """ validate password change operation and return result """
        print "HERE"
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.DATA, instance=request.user)
        
        if serializer.is_valid():
            serializer.save()
            return Response({ 'detail': (u'Password successfully changed') },
                            status=status.HTTP_200_OK)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    model = Group
    serializer_class = GroupSerializer


class MeView(GenericAPIView):
    permission_classes = ((permissions.IsAuthenticated,))
    serializer_class = User_FloatingDataSerializer

    #def pre_save(self, obj):
    #    obj.user = self.request.user

    def get(self, request):
        if request.QUERY_PARAMS.get('list')=='list':
            queryset = User_Company.objects.filter(user=request.user, is_confirmed=True)
            serializer = User_CompanySerializer(queryset, many=True)
            return Response(serializer.data)

        data=User_FloatingData.objects.filter(user=request.user)
        if len(data)==0 or not data[0].selectedPrivileges.company.is_confirmed:
            return Response({"username": request.user.username, "user_id":request.user.id, "group": request.user.groups.all()[0].name},
                            status=status.HTTP_200_OK)
        elif len(data)==1:
            return Response({"username": request.user.username, "user_id":request.user.id, "group": request.user.groups.all()[0].name, "company": data[0].selectedPrivileges.company.name,
                "companyID": data[0].selectedPrivileges.company.id,'can_converse': data[0].selectedPrivileges.can_converse, 'can_accept_inquiry': data[0].selectedPrivileges.can_accept_inquiry,
                'can_apply_inquiry': data[0].selectedPrivileges.can_apply_inquiry, 'can_post_inquiry': data[0].selectedPrivileges.can_post_inquiry,
                'can_change_company_properties': data[0].selectedPrivileges.can_change_company_properties, 
                'can_grant_company_privileges': data[0].selectedPrivileges.can_grant_company_privileges},
                            status=status.HTTP_200_OK)

    def post(self, request):
        if 'usercompanyID' in request.DATA:
            user_companyObj = User_Company.objects.filter(id=request.DATA['usercompanyID'])
            if len(user_companyObj)!=1:
                return Response({"details": "This ID doesn't exist."}, status=status.HTTP_400_BAD_REQUEST)

            data=User_FloatingData.objects.filter(user=request.user)
            if len(data)==0:
                #serializer = self.serializer_class(data={'user':request.user.username})
                serializer = self.serializer_class(data={})
            elif len(data)==1:
                queryset = User_FloatingData.objects.get(user=request.user)
                serializer = User_FloatingDataSerializer(queryset, data={})
            else:
                print "No to je pa nerodno!"
                return Response({"details": "This shouldn't happen."}, status=status.HTTP_400_BAD_REQUEST)

            if serializer.is_valid():
                serializer.object.user = request.user
                serializer.object.selectedPrivileges = user_companyObj[0]
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            
            

        return Response({"username": request.user.username, "group": request.user.groups.all()[0].name},
                        status=status.HTTP_200_OK)

class InquiryViewSet(viewsets.ModelViewSet):
    serializer_class = InquirySerializer
    model = Inquiry
    permission_classes = (permissions.IsAuthenticated,)
    #def get_permissions(self):
        # allow non-authenticated user to create via POST
    #    return (permissions.AllowAny() if self.request.method == 'POST'
    #            else IsStaffOrTargetUser()),

    def list(self, request, pk=None):
    	companyID = request.QUERY_PARAMS.get('companyID')
    	if companyID != None:
    		queryset = Inquiry.objects.filter(company__id=companyID)
    		serializer = self.get_serializer(queryset, many=True)
	        return Response(serializer.data)

        reverseSearch = request.QUERY_PARAMS.get('reverseSearch')
        name = request.QUERY_PARAMS.get('name')
        company = request.QUERY_PARAMS.get('company')
        workArea = request.QUERY_PARAMS.get('workArea')
        area = request.QUERY_PARAMS.get('area')
        activeDate = request.QUERY_PARAMS.get('activeDate')
        orderBy = request.QUERY_PARAMS.get('orderBy')
        #print reverseSearch
        if name == None:
            name=""

        if company == None:
            company=""

        if workArea == None:
            workArea=""

        if area == None:
            area=""

        if activeDate == None:
            activeDate=""

        if not (orderBy == 'name' or orderBy == 'company' or orderBy == 'workArea' or orderBy == 'area' or orderBy == 'activeDate'):
            orderBy = 'name'

        #print reverseSearch
        if reverseSearch == 'true':
            orderBy = "-"+orderBy
            
        #print orderBy


        queryset = Inquiry.objects.filter(name__icontains=name, company__name__icontains=company, workArea__icontains=workArea, area__icontains=area, activeDate__icontains=activeDate).order_by(orderBy)
        print queryset
        paginator = Paginator(queryset, 10)


        page = request.QUERY_PARAMS.get('page')
        try:
            inquiries = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            inquiries = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999),
            # deliver last page of results.
            inquiries = paginator.page(paginator.num_pages)

        serializer_context = {'request': request}
        #serializer = self.get_serializer(queryset, many=True)

        serializer = PaginatedInquirySerializer(inquiries,
                                         context=serializer_context)
        return Response(serializer.data)

    def create(self, request):
        #print request.DATA['activeDate']
        #print datetime.strptime(request.DATA['activeDate'], '%Y-%m-%dT%r').date()
        #del request.DATA['activeDate']
        serializer = self.get_serializer(data=request.DATA)
        if serializer.is_valid():
            serializer.object.creator = self.request.user
            userData = User_FloatingData.objects.get(user=request.user)
            serializer.object.company = userData.selectedPrivileges.company
            serializer.save()
            
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        #print type(int(pk))
        queryset = Inquiry.objects.all()
        try:
            nothing = int(pk)
            inquiry = get_object_or_404(queryset, pk=pk)
        except:
            inquiry = get_object_or_404(queryset, name=pk)
            pass
        
        #company = get_object_or_404(queryset, pk=pk)
        serializer = InquirySerializer(inquiry)
        return Response(serializer.data)

class OfferViewSet(viewsets.ModelViewSet):
    serializer_class = OfferSerializer
    model = Offer
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, pk=None):

        print request.QUERY_PARAMS
        if request.QUERY_PARAMS.get('inquiryID') != "":
            queryset = Offer.objects.filter(inquiry__id=request.QUERY_PARAMS.get('inquiryID'))
        elif request.QUERY_PARAMS.get('companyID') != "":
            queryset = Offer.objects.filter(company__id=request.QUERY_PARAMS.get('companyID'))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        print "hEREEEEEEEEEEEEEEEEEEEE!"
        serializer = self.get_serializer(queryset, many=True)
        print serializer.data
        return Response(serializer.data)

    def create(self, request):
        #inquiry = Inquiry.objects.get(id=request.DATA['inquiry'])
        serializer = self.get_serializer(data=request.DATA)
        if serializer.is_valid():
            inquiry = Inquiry.objects.get(id=request.DATA['inquiry']);
            serializer.object.inquiry = inquiry
            serializer.object.inquiryID = inquiry
            serializer.object.creator = self.request.user
            userData = User_FloatingData.objects.get(user=request.user)
            serializer.object.company = userData.selectedPrivileges.company
            serializer.save()
            print "hERE!"
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)