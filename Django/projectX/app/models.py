from django.db import models
#from django.contrib.auth.models import User

class Company(models.Model):
	companyCreator = models.ForeignKey('auth.User', related_name='companies')
	companyType = models.CharField(blank=True, max_length=200)
	inquiryJobsNumber = models.IntegerField(default=0)
	inquiryRating = models.FloatField(default=0)
	workArea = models.CharField(blank=True, max_length=500)
	description = models.CharField(blank=True, max_length=3000)
	address = models.CharField(max_length=100)
	name = models.CharField(max_length=100)
	created = models.DateTimeField(auto_now_add=True)
	is_confirmed = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)


class User_Company(models.Model):
	user = models.ForeignKey('auth.User', related_name='company_users')
	company = models.ForeignKey(Company, related_name='user_companies')
	is_confirmed = models.BooleanField(default=False)
	can_converse = models.BooleanField(default=False)
	can_accept_inquiry = models.BooleanField(default=False)
	can_apply_inquiry = models.BooleanField(default=False)
	can_post_inquiry = models.BooleanField(default=False)
	can_change_company_properties = models.BooleanField(default=False)
	can_grant_company_privileges = models.BooleanField(default=False)

class User_FloatingData(models.Model):
	selectedPrivileges = models.ForeignKey(User_Company, related_name='user_floatingdata', blank=True)
	user = models.ForeignKey('auth.User', related_name='user_floatingdata')
	language = models.CharField(max_length=30, default='en')

class Inquiry(models.Model):
	name = models.CharField(max_length=100)
	workArea = models.CharField(blank=True, max_length=500)
	details = models.CharField(blank=True, max_length=3000)
	activeDate = models.DateField()
	area = models.CharField(blank=True, max_length=100)
	creator = models.ForeignKey('auth.User', related_name='inquiries')
	company = models.ForeignKey(Company, related_name='inquiries')

class Offer(models.Model):
	description = models.CharField(blank=True, max_length=3000)
	is_accepted = models.BooleanField(default=False)
	is_offerer_confirmed = models.BooleanField(default=False)
	is_inquirier_confirmed = models.BooleanField(default=False)
	offerer_has_rated = models.BooleanField(default=False)
	inquirier_has_rated = models.BooleanField(default=False)
	creator = models.ForeignKey('auth.User', related_name='offers')
	company = models.ForeignKey(Company, related_name='offers')
	inquiry = models.ForeignKey(Inquiry, related_name='offers')
	inquiryID = models.ForeignKey(Inquiry, related_name='offers_by_id')