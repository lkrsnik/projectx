from django.forms import widgets
from rest_framework import serializers, pagination
from app.models import Company, User_Company, User_FloatingData, Inquiry, Offer
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('id', 'username', 'password', 'first_name', 'last_name', 'email', 'groups', 'is_staff')
		write_only_fields = ('password',)
	def restore_object(self, attrs, instance=None):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
		user = super(UserSerializer, self).restore_object(attrs, instance)
		user.set_password(attrs['password'])
		return user

class ChangePasswordSerializer(serializers.Serializer):
    """
    Change password serializer
    """
    current_password = serializers.CharField(
        max_length=70,
        #required=False  # optional because users subscribed from social network won't have a password set
    )
    password1 = serializers.CharField(
        max_length=70
    )
    password2 = serializers.CharField(
        max_length=70
    )
    
    def validate_current_password(self, attrs, source):
        """
        current password check
        """
        if self.object.has_usable_password() and not self.object.check_password(attrs.get("current_password")):
            raise serializers.ValidationError(('Current password is not correct'))
        
        return attrs
    
    def validate_password2(self, attrs, source):
        """
        password_confirmation check
        """
        password_confirmation = attrs[source]
        password = attrs['password1']
        
        if password_confirmation != password:
            raise serializers.ValidationError(('Password confirmation mismatch'))
        
        return attrs
    
    def restore_object(self, attrs, instance=None):
        """ change password """
        if instance is not None:
            instance.set_password(attrs.get('password2'))
            return instance
        
        return User(**attrs)

class AuthenticateSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=30)
    password = serializers.CharField(max_length=128)

class GetMeSerializer(serializers.ModelSerializer):
    class Meta:
		model = User
		fields = ('groups', 'username')

class UserModifySerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('id', 'username', 'first_name', 'last_name', 'email',)
		read_only_fields = ('id',)

class User_FloatingDataSerializer(serializers.HyperlinkedModelSerializer):
	user = serializers.Field(source='user.username')
	selectedPrivileges = serializers.Field(source='selectedPrivileges.id')
	class Meta:
		model = User_FloatingData
		fields = ('id', 'user', 'selectedPrivileges','language')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Group
		fields = ('name',)
	
# class CompanySerializer(serializers.HyperlinkedModelSerializer):
# 	companyCreator = serializers.Field(source='companyCreator.username')
# 	class Meta:
# 		model = Company
# 		fields = ('created','name','address','workArea', 'companyCreator')

class CompanySerializer(serializers.HyperlinkedModelSerializer):
	companyCreator = serializers.Field(source='companyCreator.username')
	class Meta:
		model = Company
		fields = ('id', 'companyCreator','inquiryJobsNumber','inquiryRating', 'workArea', 'description', 'address', 'name', 'is_confirmed')
		read_only_fields = ('id',)

class CompanyApprovementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'is_confirmed')
        read_only_fields = ('id',)

class PaginatedCompanySerializer(pagination.PaginationSerializer):
    """
    Serializes page objects of user querysets.
    """
    class Meta:
        object_serializer_class = CompanySerializer

class PaginatedUsersSerializer(pagination.PaginationSerializer):
    """
    Serializes page objects of user querysets.
    """
    class Meta:
        object_serializer_class = UserSerializer

class User_CompanySerializer(serializers.HyperlinkedModelSerializer):
	user = serializers.Field(source='user.username')
	company = serializers.Field(source='company.name')
	class Meta:
		model = User_Company
		fields = ('id','user', 'company', 'is_confirmed', 'can_converse', 'can_accept_inquiry',
		 'can_apply_inquiry', 'can_post_inquiry', 'can_change_company_properties', 'can_grant_company_privileges')

class InquirySerializer(serializers.HyperlinkedModelSerializer):
	creator = serializers.Field(source='creator.username')
	company = serializers.Field(source='company.name')
	class Meta:
		model = Inquiry
		fields = ('id', 'name', 'workArea', 'details', 'activeDate', 'area', 'creator', 'company',)
		read_only_fields = ('id',)
#InquirySerializer.base_fields['company'] = CompanySerializer()

class PaginatedInquirySerializer(pagination.PaginationSerializer):
    """
    Serializes page objects of user querysets.
    """
    class Meta:
        object_serializer_class = InquirySerializer

class OfferSerializer(serializers.HyperlinkedModelSerializer):
    creator = serializers.Field(source='creator.username')
    company = serializers.Field(source='company.name')
    inquiry = serializers.Field(source='inquiry.name')
    inquiryID = serializers.Field(source='inquiry.id')
    class Meta:
        model = Offer
        fields = ('id', 'description', 'is_accepted', 'is_offerer_confirmed', 'is_inquirier_confirmed',
         'offerer_has_rated', 'inquirier_has_rated', 'creator', 'company', 'inquiry','inquiryID',)
        read_only_fields = ('id',)
# class LoginSerializer(serializers.Serializer):
#     username = serializers.CharField(max_length=30)
#     password = serializers.CharField(max_length=128)

# class TokenSerializer(serializers.ModelSerializer):

#     """
#     Serializer for Token model.
#     """

#     class Meta:
#         model = Token
#         fields = ('key',)