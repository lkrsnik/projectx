from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from app import views

urlpatterns = format_suffix_patterns(patterns('app.views',
    # url(r'^companies/$', views.CompanyList.as_view(), name='company-list'),
    # url(r'^companies/(?P<pk>[0-9]+)/$', views.CompanyDetail.as_view(), name='company-detail'),
    # url(r'^users/$', views.UserList.as_view(), name='user-list'),
    # url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),
    #url(r'^login/$', views.Login.as_view(), name='rest_login'),
    url(r'^me/$', views.MeView.as_view(), name='rest_me'),
    url(r'^password/$', views.AccountPassword.as_view(), name='rest_pass'),
    #url(r'^logout/$', views.Logout.as_view(), name='rest_logout'),
    url(r'^company_privileges/$', views.User_CompanyView.as_view(), name='company_privileges'),
    url(r'^company_approvement/$', views.CompanyApprovementView.as_view(), name='company_approvement'),
))
