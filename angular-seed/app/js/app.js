'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'ui.bootstrap',
  'ui.router',
  'gettext',
  'restangular',
  'ngRoute',
]).
config(['$routeProvider', 'RestangularProvider', '$httpProvider', function($routeProvider,RestangularProvider,$httpProvider) {
  // Static views (menu etc.)
  // $stateProvider
  // 	.state('state1', {
  //     templateUrl: "partials/state1.html"
  //   })	
  //   .state('state2', {
  //     templateUrl: "partials/state2.html"
  //   })
  $routeProvider.when('/news', {templateUrl: 'partials/news.html', controller: 'NewsCtrl'});
  $routeProvider.when('/register', {templateUrl: 'partials/register.html', controller: 'RegisterCtrl'});
  $routeProvider.when('/signin', {templateUrl: 'partials/signin.html', controller: 'SignInCtrl'});
  $routeProvider.when('/profile', {templateUrl: 'partials/profiles.html', controller: 'ProfilesCtrl'})
      .when('/profile/:profileID', {templateUrl: 'partials/profile.html', controller: 'ProfileCtrl'})
  		.when('/profile/:profileID/modify', {templateUrl: 'partials/profileUpdate.html', controller: 'ProfileUpdateCtrl'})
  		.when('/password', {templateUrl: 'partials/passwordUpdate.html', controller: 'PasswordUpdateCtrl'});
  $routeProvider.when('/changecompany', {templateUrl: 'partials/changeCompany.html', controller: 'ChangeCompanyCtrl'});
  $routeProvider.when('/companies', {templateUrl: 'partials/companies.html', controller: 'CompaniesCtrl'})
        .when('/companies/:companyID', {templateUrl: 'partials/companyDetail.html', controller: 'CompanyDetailCtrl'})
        .when('/companies/:companyID/modify', {templateUrl: 'partials/companyModify.html', controller: 'CompanyModifyCtrl'})
        .when('/companies/:companyID/:offerID', {templateUrl: 'partials/offerDetail.html', controller: 'OfferDetailCtrl'});
  $routeProvider.when('/companyregister', {templateUrl: 'partials/companyRegister.html', controller: 'CompanyRegisterCtrl'});
  $routeProvider.when('/inquiries', {templateUrl: 'partials/inquiries.html', controller: 'InquiriesCtrl'})
      .when('/inquiries/:inquiryID', {templateUrl: 'partials/inquiryDetail.html', controller: 'InquiryDetailCtrl'})
      .when('/inquiries/:inquiryID/createoffer', {templateUrl: 'partials/offerCreate.html', controller: 'OfferCreateCtrl'})
      .when('/inquiries/:inquiryID/modify', {templateUrl: 'partials/inquiryModify.html', controller: 'InquiryModifyCtrl'})
      .when('/inquiries/:inquiryID/contract', {templateUrl: 'partials/contract.html', controller: 'ContractCtrl'})
      .when('/inquiries/:inquiryID/contract/modify', {templateUrl: 'partials/contractModify.html', controller: 'ContractModifyCtrl'})
      .when('/inquiries/:inquiryID/:offerID', {templateUrl: 'partials/offerDetail.html', controller: 'OfferDetailCtrl'})
      .when('/inquiries/:inquiryID/:offerID/modify', {templateUrl: 'partials/offerModify.html', controller: 'OfferModifyCtrl'});
  $routeProvider.when('/inquirycreate', {templateUrl: 'partials/inquiryCreate.html', controller: 'InquiryCreateCtrl'});

  $routeProvider.otherwise({redirectTo: '/news'});


  $httpProvider.interceptors.push('authInterceptor');
  RestangularProvider.setBaseUrl('http://localhost:8080/');
  RestangularProvider.setRequestSuffix('/');
  //RestangularProvider.setDefaultRequestParams('get', {limit: 10});
}]).
run(function (gettextCatalog) {
	//gettextCatalog.currentLanguage = 'sl';
	

	//console.log("HERE!");
	gettextCatalog.debug=true;
})
;
