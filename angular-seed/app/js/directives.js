'use strict';

/* Directives */


var app = angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);

//Checks wheather field contains only characters and specific simbols
var NAME_REGEXP = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžđÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽĐ∂ð ,.'-]+$/;
app.directive('personalnames', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (NAME_REGEXP.test(viewValue) || viewValue=='') {
          // it is valid
          ctrl.$setValidity('personalnames', true);
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('personalnames', false);
        }
        return viewValue;
      });
    }
  };
});

var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
app.directive('emailtest', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (EMAIL_REGEXP.test(viewValue) || viewValue=='') {
          // it is valid
          ctrl.$setValidity('emailtest', true);
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('emailtest', false);
        }
        return viewValue;
      });
    }
  };
});

app.directive('match', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            match: '='
        },
        link: function(scope, elem, attrs, ctrl) {
            scope.$watch(function() {
                return (ctrl.$pristine && angular.isUndefined(ctrl.$modelValue)) || scope.match === ctrl.$modelValue;
            }, function(currentValue) {
                ctrl.$setValidity('match', currentValue);
            });
        }
    };
});

// app.directive('uniqueusername', ['Restangular', function(Restangular) {
//   return {
//     require:'ngModel',
//     restrict:'A',
//     link:function (scope, el, attrs, ctrl) {

//       //TODO: We need to check that the value is different to the original

//       //using push() here to run it as the last parser, after we are sure that other validators were run
//       ctrl.$parsers.push(function (viewValue) {
//         if (viewValue) {
//         	Restangular.all('users').customGET("", {username: viewValue}).then(function(theobject) {
//             if(theobject.length>0){
//               ctrl.$setValidity('uniqueusername', false);
//             } else {
//               ctrl.$setValidity('uniqueusername', true);
//             }
//             return theobject;
//           });
//           //Users.query({email:viewValue}, function (users) {
//           //  if (users.length === 0) {
//           //    ctrl.$setValidity('uniqueEmail', true);
//           //  } else {
//           //    ctrl.$setValidity('uniqueEmail', false);
//            // }
//           //});
//           return viewValue;
//         }
//       });
//     }
//   };
// }]);