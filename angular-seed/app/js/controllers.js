'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('NewsCtrl', [function($scope, USER_ROLES, Session, AuthService, $window, $location) {

  }])
  .controller('ApplicationCtrl', ['$scope', 'USER_ROLES', 'Session', 'AuthService', '$window', '$location', function($scope, USER_ROLES, Session, AuthService, $window, $location) {
    $scope.updateSessionView = function () {
      AuthService.refresh().then(function (user) {
      	$scope.currentUser = Session.username;
        $scope.userRole = Session.userRole;
        $scope.selectedCompany = Session.company;
        $scope.selectedCompanyID = Session.companyID;
	    $scope.can_accept_inquiry = Session.can_accept_inquiry;
	    $scope.can_apply_inquiry = Session.can_apply_inquiry;
	    $scope.can_post_inquiry = Session.can_post_inquiry;
	    $scope.can_change_company_properties = Session.can_change_company_properties;
	    $scope.can_grant_company_privileges = Session.can_grant_company_privileges;
	  }, function () {
		$scope.currentUser = "";
        $scope.userRole = "";
        $scope.selectedCompany = "";
        $scope.selectedCompanyID = "";
        $scope.can_accept_inquiry = "";
	    $scope.can_apply_inquiry = "";
	    $scope.can_post_inquiry = "";
	    $scope.can_change_company_properties = "";
	    $scope.can_grant_company_privileges = "";
	  });
    };

    $scope.profile = function () {
      $scope.updateSessionView();
      $location.path("/profile/"+Session.user_id);
    };

    $scope.logout = function () {
    	delete $window.localStorage.token;
    	$scope.updateSessionView();
    	$location.path("/signin/");
    };

    $scope.updateSessionView();
  }])
  .controller('RegisterCtrl', ['$scope', 'Restangular', function($scope, Restangular) {
    $scope.result = 'none';
    $scope.update = function(user) {
      //user.removeAttr('repeatPassword');
      Restangular.all('accounts/').post(user).then(function() {
        $scope.result = "success";
      }, function(response) {
        if(response.status === 400){
          $scope.result = 'notunique';
        }
      });
    };
  }])
  .controller('SignInCtrl', ['$scope', '$rootScope', 'AUTH_EVENTS', 'AuthService', '$window', '$location', function($scope, $rootScope, AUTH_EVENTS, AuthService, $window, $location) {
    $scope.credentials = {
      username: '',
      password: ''
    };
    $scope.login = function (credentials) {
       AuthService.login(credentials).then(function (token) {

          $window.localStorage.token = token.token;
          $scope.updateSessionView();
          $location.path("/companies/");

       }, function () {
          delete $window.localStorage.token;
          $scope.result = 'unable';
       });
    };
  }])
  .controller('ProfileCtrl', ['$scope', 'ProfileService', '$routeParams', '$location', function($scope, ProfileService, $routeParams, $location) {
    ProfileService.getProfile($routeParams.profileID).then(function (user) {
        $scope.user=user;
    });
    $scope.modify = function (goTo) {
      if (goTo=="profile")
      	$location.path("/profile/"+$routeParams.profileID+"/modify");
      else if (goTo=="password")
      	$location.path("/password");
    }

  }])
  .controller('ProfilesCtrl', ['$scope', 'ProfileService', '$location', function($scope, ProfileService, $location) {
  	$scope.reverseSearch = false;
  	$scope.currentPage = 1;
    $scope.totalPages = 1;
    $scope.filters="";
    $scope.filters.username = "";
    $scope.filters.email = "";
    $scope.filters.first_name = "";
    $scope.filters.last_name = "";

    $scope.details = function (id) {
      
      $location.path("/profile/"+id);
    };

    $scope.next = function () {
       if($scope.currentPage != $scope.totalPages){
        $scope.currentPage++;
        $scope.refresh();
       }
    };
    $scope.previous = function () {
       if($scope.currentPage != 1){
        $scope.currentPage--
        $scope.refresh();
       }
    };

    $scope.order = function (orderName) {

      if(orderName == $scope.orderBy)
      	$scope.reverseSearch = ($scope.reverseSearch && !true) || (!$scope.reverseSearch && true);
      $scope.orderBy = orderName;
      $scope.refresh();
      
    };

    $scope.refresh = function () {
      ProfileService.getAllProfiles($scope.currentPage, $scope.filters, $scope.orderBy, $scope.reverseSearch).then(function (users) {
        $scope.users = users.results;
        $scope.totalPages = Math.ceil(users.count/10)
      });
    };
    $scope.refresh();
  }])
  .controller('ProfileUpdateCtrl', ['$scope', '$routeParams', 'ProfileService', function($scope, $routeParams, ProfileService) {
    var refresh = function () {
	    ProfileService.getProfile($routeParams.profileID).then(function (user) {
	        $scope.user=user;
	        switch(user.groups[0]) {
	          case 1:
	            $scope.roleRadios = 'admin';
	            break;
	          case 2:
	            $scope.roleRadios = 'editor';
	            break;
	          case 3:
	            $scope.roleRadios = 'regular';
	            break;
	          default:
	            break;
	        }
	    });
	};
    $scope.roleRadios = 'regular';
    $scope.update = function (user) {
      ProfileService.updateProfile(user, $scope.roleRadios).then(function (recUser) {
      	if (recUser.statusText=="BAD REQUEST"){
      		$scope.result = 'failure';
      	} else {
      		$scope.user=recUser;
	        refresh();
	        $scope.result = "success";
      	}
        
      }, function(response) {
          $scope.result = 'failure';
      });
    };
    refresh();
  }])
  .controller('PasswordUpdateCtrl', ['$scope', '$routeParams', 'ProfileService', function($scope, $routeParams, ProfileService) {
    $scope.update = function (passwords) {
      ProfileService.change(passwords).then(function (recUser) {
        $scope.result = "success";
      }, function(response) {
        if(response.status === 400){
          $scope.result = 'notunique';
        }
      });
    }
  }])
  .controller('ChangeCompanyCtrl', ['$scope','MeService', 'AuthService', '$location', 'CompanyService', function($scope, MeService, AuthService, $location, CompanyService) {
  	$scope.select = function (user_companyID, companyName) {
      MeService.createCompanyRelation(user_companyID).then(function (companies) {
      	AuthService.refresh();
      	$scope.updateSessionView();
      	CompanyService.getDetail(companyName).then(function (company) {
          $location.path("/companies/"+company.id);
        }, function () {
          //$location.path("/inquiries");
        });
      	//$location.path("/companies/"+$scope.selectedCompanyID);
      });
    };

  	var refresh = function () {
      MeService.getCompanies().then(function (companies) {
        $scope.companies = companies;
      });
    };
    refresh();
  }])
  .controller('CompaniesCtrl', ['$scope', 'CompanyService', '$location', function($scope, CompanyService, $location) {
    $scope.reverseSearch = false;
    $scope.currentPage = 1;
    $scope.totalPages = 1;
    $scope.filters="";
    $scope.filters.tname = "";
    $scope.filters.address = "";
    $scope.filters.workArea = "";

    $scope.details = function (id) {
      $location.path("/companies/"+id);
    };

    $scope.next = function () {
       if($scope.currentPage != $scope.totalPages){
        $scope.currentPage++
        $scope.refresh();
       }
    };
    $scope.previous = function () {
       if($scope.currentPage != 1){
        $scope.currentPage--
        $scope.refresh();
       }
    };

    $scope.order = function (orderName) {
      if(orderName == $scope.orderBy)
        $scope.reverseSearch = ($scope.reverseSearch && !true) || (!$scope.reverseSearch && true);
      $scope.orderBy = orderName;
      $scope.refresh();
      
    };

    $scope.refresh = function () {
      CompanyService.getList($scope.currentPage, $scope.filters, $scope.orderBy, $scope.reverseSearch).then(function (companies) {
        $scope.companies = companies.results;
        $scope.totalPages = Math.ceil(companies.count/10)
      });
    };
    $scope.refresh();
    
  }])
  .controller('CompanyModifyCtrl', ['$scope','$routeParams', '$location', 'CompanyService', function($scope, $routeParams, $location, CompanyService) {
    
    $scope.modify = function (company) {
       CompanyService.update(company).then(function (res) {
          if (res == undefined){
            $scope.result = "success";
          } else {
            $scope.result = "failure";
          }
          
       }, function () {
         $scope.result = "failure";
       });
    };

    var refresh = function () {
      CompanyService.getDetail($routeParams.companyID).then(function (company) {
        $scope.company = company;
      }, function () {
        $location.path("/companies");
      });
    };
    refresh();
  }])
  .controller('CompanyDetailCtrl', ['$scope','$routeParams', 'CompanyService', 'InquiryService', 'OfferService', '$location', 'CompanyPrivilegeService', 'Session', 'MeService', function($scope, $routeParams, CompanyService, InquiryService, OfferService, $location, CompanyPrivilegeService, Session, MeService) {
    //
    $scope.max = 10;
    $scope.isReadonly = true;
    $scope.change_properties = Session.can_change_company_properties
    $scope.company_is_confirmed = false;
    $scope.see_approve=true;

    $scope.toInquiry = function (inquiryID) {
      $location.path("/inquiries/"+inquiryID);
    };

    $scope.approve = function () {
      CompanyService.postApprovement($routeParams.companyID).then(function (approvement) {
      	$scope.result = 'companyApproveOK';
      	$scope.see_approve=false;
      }, function () { $scope.result = 'companyApproveNOK'; });
      refresh();
    };

    $scope.modify = function () {
      $location.path("/companies/"+$routeParams.companyID+"/modify");
    };

    $scope.modifyPrivileges = function (privilege) {
    	$scope.result = 'approveOK';
    	$scope.changedUser = privilege.user;
      CompanyPrivilegeService.update(privilege).then(function () {
      }, function () { $scope.result = 'approveNOK'; $scope.changedUser = privilege.user; });
    };


    $scope.join = function () {
      CompanyPrivilegeService.create($routeParams.companyID).then(function () {
      	$scope.result = 'workHereOK';
      }, function () { $scope.result = 'workHereNOK'; });

    };

    $scope.details = function (offer) {
      if(offer.is_accepted)
        $location.path("/inquiries/"+offer.inquiryID+"/contract");
      else
        $location.path("/inquiries/"+offer.inquiryID+"/"+offer.id);

    };

    var refresh = function () {
      CompanyService.getDetail($routeParams.companyID).then(function (company) {
        $scope.company = company;
        $scope.rateShow = Math.round(company.inquiryRating);
        $scope.rateNum = Math.round(company.inquiryRating*100)/100;
        MeService.getCompanies().then(function (companies) {
	        $scope.is_in_company = true;
	        for(var i=0; i<companies.length;i++){
	        	if(companies[i].company == $scope.company.name)
	        		$scope.is_in_company = false;
	        }
	      });
      }, function () {
        $location.path("/companies");
      });

      CompanyService.getApprovement($routeParams.companyID).then(function (approvement) {
      	$scope.company_is_confirmed = approvement[0].is_confirmed;
      }, function () { });

      CompanyPrivilegeService.get($routeParams.companyID).then(function (thisPrivileges) {
        $scope.privileges = thisPrivileges
      }, function () {
        
      });

      InquiryService.getAllInquiriesByCompany($routeParams.companyID).then(function (inquiries) {
        $scope.inquiries = inquiries;
      });

      OfferService.getAllOffers("", $routeParams.companyID).then(function (offers) {
      	$scope.offers = offers;
      	$scope.is_in_offer = true;
        for(var i=0; i<offers.length;i++){
        	if(offers[i].company == $scope.selectedCompany)
        		$scope.is_in_offer = false;
        }

      }, function () {
        $location.path("/companies");
      });
      

    };
    refresh();
  }])
  .controller('CompanyRegisterCtrl', ['$scope', 'CompanyService', function($scope, CompanyService) {
    $scope.result = 'none';
    $scope.company = {
      name: '',
      address: '',
      description: '',
      workArea: ''
    };
    $scope.create = function (company) {
       CompanyService.create(company).then(function () {

         $scope.result = "success";
       }, function () {
         $scope.result = "failure";
       });
    };
  }])
  .controller('InquiriesCtrl', ['$scope', 'InquiryService', '$location', function($scope, InquiryService, $location) {
  	$scope.reverseSearch = false;
  	$scope.currentPage = 1;
    $scope.totalPages = 1;
    $scope.filters="";
    $scope.filters.username = "";
    $scope.filters.email = "";
    $scope.filters.first_name = "";
    $scope.filters.last_name = "";

    $scope.details = function (id) {
      $location.path("/inquiries/"+id);
    };

    $scope.next = function () {
       if($scope.currentPage != $scope.totalPages){
        $scope.currentPage++;
        $scope.refresh();
       }
    };
    $scope.previous = function () {
       if($scope.currentPage != 1){
        $scope.currentPage--
        $scope.refresh();
       }
    };

    $scope.order = function (orderName) {

      if(orderName == $scope.orderBy)
      	$scope.reverseSearch = ($scope.reverseSearch && !true) || (!$scope.reverseSearch && true);
      $scope.orderBy = orderName;
      $scope.refresh();
      
    };

    $scope.refresh = function () {
      InquiryService.getAllInquiries($scope.currentPage, $scope.filters, $scope.orderBy, $scope.reverseSearch).then(function (inquiries) {
        $scope.inquiries = inquiries.results;
        $scope.totalPages = Math.ceil(inquiries.count/10);
      });
    };
    $scope.refresh();
  }])
  .controller('InquiryCreateCtrl', ['$scope', 'InquiryService', function($scope, InquiryService) {
  	$scope.inquiry = {
      name: '',
      activeDate: '',
      workArea: '',
      details: '',
      area: '',
      
      
    };

  	//DATA FOR CALENDAR
  	// --------------------------------
	$scope.today = function() {
	  $scope.inquiry.activeDate = new Date();
	};
	$scope.today(); 
	$scope.toggleMin = function() {
	  $scope.minDate = $scope.minDate ? null : new Date();
	}; 
	$scope.toggleMin();
	$scope.open = function($event) {
	  $event.preventDefault();
	  $event.stopPropagation();

	  $scope.opened = true;
	};

	$scope.dateOptions = {
	  formatYear: 'yy',
	  startingDay: 1
	};
	$scope.initDate = new Date('2016-15-20');
	$scope.format = 'dd-MMMM-yyyy';

	//---------------------------------

	$scope.result = 'none';
    $scope.create = function (inquiry) {
    	var bla = inquiry.activeDate;
       InquiryService.create(inquiry).then(function () {

         $scope.result = "success";
       }, function () {
         $scope.result = "failure";
       });
    };
  }])
  .controller('LanguageCtrl', ['$scope', 'gettextCatalog' , function($scope, gettextCatalog) {
    $scope.languages = ['en','sl'];
    $scope.lang = 'en';
    $scope.changeLanguage = function(lang){
      gettextCatalog.setCurrentLanguage(lang);
      $scope.lang = lang;
    };
  }])
  .controller('InquiryDetailCtrl', ['$scope', 'InquiryService','CompanyService', '$routeParams', '$location', 'OfferService', function($scope, InquiryService, CompanyService, $routeParams, $location, OfferService) {
  	$scope.max = 10;
    $scope.isReadonly = true;

    $scope.modify = function () {
      $location.path("/inquiries/"+$routeParams.inquiryID+"/modify");
    };

    $scope.details = function (offerID) {
      $location.path("/inquiries/"+$routeParams.inquiryID+"/"+offerID);
    };

    $scope.toContract = function () {
      $location.path("/inquiries/"+$routeParams.inquiryID+"/contract");
    };

    $scope.sendOffer = function () {
      $location.path("/inquiries/"+$routeParams.inquiryID+"/createoffer");
    };

  	var refresh = function () {
      InquiryService.getDetail($routeParams.inquiryID).then(function (inquiry) {
      	$scope.inquiry = inquiry;
        
        CompanyService.getDetail(inquiry.company).then(function (company) {
          $scope.company = company;
          $scope.rateShow = Math.round(company.inquiryRating);
          $scope.rateNum = Math.round(company.inquiryRating*100)/100;
        }, function () {
          $location.path("/inquiries");
        });

      }, function () {
        $location.path("/inquiries");
      });

      OfferService.getAllOffers($routeParams.inquiryID, "").then(function (offers) {
      	$scope.offers = offers;
      	$scope.is_in_offer = true;
        $scope.contract_online = false;
        for(var i=0; i<offers.length;i++){
        	if(offers[i].company == $scope.selectedCompany)
        		$scope.is_in_offer = false;
          if(offers[i].is_accepted)
            $scope.contract_online=true;
        }

      }, function () {
        $location.path("/inquiries");
      });

    };
    refresh();
  }])
  .controller('InquiryModifyCtrl', ['$scope','$routeParams', '$location', 'InquiryService', function($scope, $routeParams, $location, InquiryService) {
    //DATA FOR CALENDAR
    // --------------------------------
    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    }; 
    $scope.toggleMin();
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };
    $scope.initDate = new Date('2016-15-20');
    $scope.format = 'dd-MMMM-yyyy';

  //---------------------------------

    $scope.modify = function (inquiry) {
       
       InquiryService.update(inquiry).then(function (res) {
          $location.path("/inquiries/"+$routeParams.inquiryID);
          $scope.result = "success";
          
       }, function () {
         $scope.result = "failure";
       });
    };

    var refresh = function () {
      InquiryService.getDetail($routeParams.inquiryID).then(function (inquiry) {
        $scope.inquiry = inquiry;
      }, function () {
      });
    };
    refresh();
  }])
  .controller('OfferDetailCtrl', ['$scope', 'OfferService', 'InquiryService', 'CompanyService', '$routeParams', '$location', function($scope, OfferService, InquiryService, CompanyService, $routeParams, $location) {
    $scope.max = 10;
    $scope.isReadonly = true;

    $scope.modify = function () {
      $location.path($location.path()+"/modify");
    };

	$scope.acceptOffer = function (offer) {
	  offer.is_accepted = true;
      OfferService.update(offer).then(function (res) {
      	$location.path("/inquiries/"+$routeParams.inquiryID+"/contract");
        $scope.result = "success";  
      }, function () {
       $scope.result = "failure";
      });
    };
    var refresh = function () {
      OfferService.getDetail($routeParams.offerID).then(function (offer) {
      	$scope.offer = offer;
        
        CompanyService.getDetail(offer.company).then(function (company) {
          $scope.offer_company = company;
          $scope.offer_rateShow = Math.round(company.inquiryRating);
          $scope.offer_rateNum = Math.round(company.inquiryRating*100)/100;
        }, function () {
          //$location.path("/inquiries/"+$routeParams.inquiryID);
        });
        InquiryService.getDetail(offer.inquiryID).then(function (inquiry) {
          $scope.inquiry = inquiry;
          CompanyService.getDetail(inquiry.company).then(function (company) {
            $scope.inquiry_company = company;
            $scope.inquiry_rateShow = Math.round(company.inquiryRating);
            $scope.inquiry_rateNum = Math.round(company.inquiryRating*100)/100;
          }, function () {
            $location.path("/inquiries");
          });

        }, function () {
          $location.path("/inquiries");
        });

      }, function () {
        //$location.path("/inquiries/"+$routeParams.inquiryID);
      });

    };
    refresh();
  }])
  .controller('OfferCreateCtrl', ['$scope', 'OfferService', '$routeParams', '$location', function($scope, OfferService, $routeParams, $location) {
  	$scope.result = 'none';
    $scope.create = function (offer) {
       OfferService.create(offer, $routeParams.inquiryID).then(function (res) {
       	 $location.path("/inquiries/"+$routeParams.inquiryID+"/"+res.id);
         $scope.result = "success";
       }, function () {
         $scope.result = "failure";
       });
    };
  }])
  .controller('OfferModifyCtrl', ['$scope','$routeParams', '$location', 'OfferService', function($scope, $routeParams, $location, OfferService) {
    $scope.modify = function (offer) {
       
       OfferService.update(offer).then(function (res) {
          $location.path("/inquiries/"+$routeParams.inquiryID+"/"+res.id);
          $scope.result = "success";
          
       }, function () {
         $scope.result = "failure";
       });
    };

    var refresh = function () {
      OfferService.getDetail($routeParams.offerID).then(function (offer) {
        $scope.offer = offer;
      }, function () {
        //$location.path("/companies");
      });
    };
    refresh();
  }])
  .controller('ContractCtrl', ['$scope', 'OfferService', 'InquiryService', 'CompanyService', '$routeParams', '$location', function($scope, OfferService, InquiryService, CompanyService, $routeParams, $location) {

    $scope.max = 10;
    $scope.isReadonly = true;
    $scope.rateShow = 1;

    $scope.rate = function () {
      var theCompany=null;
      if($scope.selectedCompany == $scope.offer_company.name){
        theCompany = $scope.inquiry_company;
        $scope.offer.offerer_has_rated = true;
      }else if($scope.selectedCompany == $scope.inquiry_company.name){
        theCompany = $scope.offer_company;
        $scope.offer.inquirier_has_rated = true;
      }
        
      theCompany.inquiryJobsNumber = theCompany.inquiryJobsNumber+1;
      theCompany.inquiryRating = theCompany.inquiryRating*(theCompany.inquiryJobsNumber-1)/theCompany.inquiryJobsNumber+
      $scope.rateShow*1/theCompany.inquiryJobsNumber;
      
      CompanyService.updateRate(theCompany).then(function (res) {
        $scope.result = "success";
      }, function () {
       $scope.result = "failure";
      });

      OfferService.updateRate($scope.offer).then(function (res) {
        $scope.result = "success";
      }, function () {
       $scope.result = "failure";
      });

      if($scope.selectedCompany == $scope.offer_company.name){
        $scope.inquiry_company.inquiryJobsNumber = theCompany.inquiryJobsNumber;
        $scope.inquiry_rateShow = Math.round(theCompany.inquiryRating);
        $scope.inquiry_rateNum = Math.round(theCompany.inquiryRating*100)/100;
      }
      else if($scope.selectedCompany == $scope.inquiry_company.name){
        $scope.offer_company.inquiryJobsNumber = theCompany.inquiryJobsNumber;
        $scope.offer_rateShow = Math.round(theCompany.inquiryRating);
        $scope.offer_rateNum = Math.round(theCompany.inquiryRating*100)/100;
      }

      CompanyService.updateRate(theCompany).then(function (res) {
        $scope.result = "success";
      }, function () {
       $scope.result = "failure";
      });
    };

    $scope.modify = function () {
      $location.path($location.path()+"/modify");
    };

    $scope.finalSubmit = function () {
      if($scope.selectedCompany == $scope.offer_company.name)
        $scope.offer.is_offerer_confirmed = true;
      else if($scope.selectedCompany == $scope.inquiry_company.name)
        $scope.offer.is_inquirier_confirmed = true;
      OfferService.update($scope.offer).then(function (res) {
        $scope.result = "success";
      }, function () {
       $scope.result = "failure";
      });
    };

    var refresh = function () {
      OfferService.getAllOffers($routeParams.inquiryID,"").then(function (offers) {
        $scope.offer = "";
        for(var i=0; i<offers.length;i++){
          if(offers[i].is_accepted)
            $scope.offer = offers[i];
        } 
        CompanyService.getDetail($scope.offer.company).then(function (company) {
          $scope.offer_company = company;
          $scope.offer_rateShow = Math.round(company.inquiryRating);
          $scope.offer_rateNum = Math.round(company.inquiryRating*100)/100;
        }, function () {
          //$location.path("/inquiries/"+$routeParams.inquiryID);
        });
        InquiryService.getDetail($scope.offer.inquiryID).then(function (inquiry) {
          $scope.inquiry = inquiry;
          CompanyService.getDetail(inquiry.company).then(function (company) {
            $scope.inquiry_company = company;
            $scope.inquiry_rateShow = Math.round(company.inquiryRating);
            $scope.inquiry_rateNum = Math.round(company.inquiryRating*100)/100;
          }, function () {
            $location.path("/inquiries");
          });

        }, function () {
          $location.path("/inquiries");
        });

      }, function () {
        //$location.path("/inquiries/"+$routeParams.inquiryID);
      });

    };
    refresh();
  }])
  .controller('ContractModifyCtrl', ['$scope','$routeParams', '$location', 'OfferService', function($scope, $routeParams, $location, OfferService) {


    $scope.modify = function (offer) {
       offer.is_offerer_confirmed = false;
       offer.is_inquirier_confirmed = false;
       OfferService.update(offer).then(function (res) {
          $location.path("/inquiries/"+$routeParams.inquiryID+"/contract");
          $scope.result = "success";
          
       }, function () {
         $scope.result = "failure";
       });
    };

    var refresh = function () {
      OfferService.getAllOffers($routeParams.inquiryID,"").then(function (offers) {
        $scope.offer = "";
        for(var i=0; i<offers.length;i++){
          if(offers[i].is_accepted)
            $scope.offer = offers[i];
        }
      }, function () {
        //$location.path("/companies");
      });
    };
    refresh();
  }]);
