'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
//angular.module('myApp.services', ['ngResource']).
angular.module('myApp.services', []).
  value('version', '0.1')//.
  //factory('Company', ['$resource',
  //	function($resource){
  //		return $resource('http://localhost:8080/companies/:id', {}, {
  //			query: {method:'GET', params:{id:''}, isArray:true}
  //		});
  //	}

  //	]
  	
  	.constant('AUTH_EVENTS', {
	  loginSuccess: 'auth-login-success',
	  loginFailed: 'auth-login-failed',
	  logoutSuccess: 'auth-logout-success',
	  sessionTimeout: 'auth-session-timeout',
	  notAuthenticated: 'auth-not-authenticated',
	  notAuthorized: 'auth-not-authorized'
	})
	.constant('USER_ROLES', {
	  all: '*',
	  admin: 'admin',
	  editor: 'editor',
	  regular: 'regular'
	})

	.factory('AuthService', function (Restangular, Session, $log) {
	  var authService = {};
	 
	  //authService.tralala = function (credentials) {
	  //	return credentials;
	  //}
	  authService.refresh = function () {
		return Restangular.all('me/')
		.customGET()
		.then(function (res) {
		  Session.create(res);
		  return res;
		});
	  };


	   authService.login = function (credentials) {
	     return Restangular.all('api-token-auth/')
	       .post(credentials)
	       .then(function (res) {
			  return res;
	       });
	   };

	   authService.logout = function () {

	      return Restangular.one('logout')
	        .getList()
	        .then(function (res) {
	       	  console.log('Im HERE!!!' + res);
	         return res.username;
	       });
	   };
	 
	   authService.isAuthenticated = function () {
	     return !!Session.username;
	   };
	 
	   authService.isAuthorized = function (authorizedRoles) {
	     if (!angular.isArray(authorizedRoles)) {
	       authorizedRoles = [authorizedRoles];
	     }
	     return (authService.isAuthenticated() &&
	       authorizedRoles.indexOf(Session.userRole) !== -1);
	   };
	 
	  return authService;
	})

	.factory('authInterceptor', function ($rootScope, $q, $window) {
	  return {
	    request: function (config) {
	      config.headers = config.headers || {};
	      if ($window.localStorage.token) {
	        config.headers.Authorization = 'JWT ' + $window.localStorage.token;
	      }
	      return config;
	    },
	    response: function (response) {
	      if (response.status === 401) {
	        // handle the case where the user is not authenticated
	      }
	      return response || $q.when(response);
	    }
	  };
	})

	.factory('ProfileService', function (Restangular, Session, $q) {
	  var profileService = {};

	  profileService.getProfile = function (userID) {
	    return Restangular.all('accounts/')
	    .customGET(userID)
	    .then(function (res) {
	      return res;
	    });
	  };
	  profileService.updateProfile = function (user, groups) {
	    return Restangular.all('accounts')
	    .customGET(user.id)
	    .then(function (res) {
	      var result = $q.defer();
	      
	      switch(groups) {
	          case 'admin':
	            user.groups[0] = 1;
	            break;
	          case 'editor':
	            user.groups[0] = 2;
	            break;
	          case 'regular':
	            user.groups[0] = 3;
	            break;
	          default:
	            break;
	        }
	      
	      res.customPUT(user).then(function (suc) {
	      	result.resolve(suc);
	      	return suc;
	      }, function (fail) {
	        result.resolve(fail);
	        console.log(fail)
	      	return fail;
	      });
	      return result.promise;
	    });
	  };
	  profileService.change = function (pass) {
	    return Restangular.all('password/')
	    .post(pass)
	    .then(function (res) {
	      return res;
	    });
	  };

	  profileService.getAllProfiles = function (currentPage, filters, orderBy, reverse) {
	    return Restangular.all('accounts/')
	    .customGET("",{page: currentPage, username: filters.username, email: filters.email, first_name: filters.first_name, last_name: filters.last_name,
	    	orderBy: orderBy, reverseSearch: reverse})
	    .then(function (res) {
	      return res;
	    });
	  };

	  return profileService;
	})

	.factory('CompanyService', function (Restangular, Session, $log, $q) {
	  var companyService = {};
	 
	  //authService.tralala = function (credentials) {
	  //	return credentials;
	  //}
	  companyService.postApprovement = function (companyID) {
	    return Restangular.all('company_approvement/')
	    .post(companyID)
	    .then(function (res) {
	      return res;
	    });
	  };

	  companyService.getApprovement = function (companyID) {
	    return Restangular.all('company_approvement/')
	    .customGET("",{id: companyID})
	    .then(function (res) {
	      //console.log(res);
	      return res;
	    });
	  };


	  companyService.getDetail = function (companyID) {
	    return Restangular.all('companies/')
	    .customGET(companyID)
	    .then(function (res) {
	      //console.log(res);
	      return res;
	    });
	  };

	  companyService.getList = function (currentPage, filters, orderBy, reverse) {
	    return Restangular.all('companies/')
	    .customGET("",{page: currentPage, name: filters.name, address: filters.address, workArea: filters.workArea, orderBy: orderBy, reverseSearch: reverse})
	    .then(function (res) {
	      //console.log(res);
	      return res;
	    });
	  };

	  companyService.create = function (company) {
	    return Restangular.all('companies/')
	    .post(company)
	    .then(function (res) {
	      return res;
	    });
	  };

	  companyService.update = function (company) {
	    return Restangular.all('companies')
	    .customGET(company.id)
	    .then(function (res) {
	      var result = $q.defer();
	      //console.log(res);
	      res.name = company.name;
	      res.address = company.address;
	      res.workArea = company.workArea;
	      res.description = company.description;
	      res.companyCreator = company.companyCreator;
	      //console.log(res);

	      res.customPUT(company).then(function (suc) {
	      	//console.log("OK");
	      	//result=suc;
	      	result.resolve(suc);
	      	return suc;
	      }, function (suc) {
	      	//console.log(suc);
	        //result=suc;
	        result.resolve(suc);
	      	return suc;
	      });
	      return result.promise;
	    });
	  };
	  companyService.updateRate = function (company) {
	    return Restangular.all('companies')
	    .customGET(company.id)
	    .then(function (res) {
	      var result = $q.defer();

	      res.inquiryJobsNumber = company.inquiryJobsNumber;
	      res.inquiryRating = company.inquiryRating;

	      res.customPUT(company).then(function (suc) {
	      	//console.log("OK");
	      	//result=suc;
	      	result.resolve(suc);
	      	return suc;
	      }, function (suc) {
	      	//console.log(suc);
	        //result=suc;
	        result.resolve(suc);
	      	return suc;
	      });
	      return result.promise;
	    });
	  };
	  return companyService;
	})

	.factory('CompanyPrivilegeService', function (Restangular, Session, $log, $q) {
	  var companyPrivilegeService = {};

	  companyPrivilegeService.get = function (companyID) {
	    return Restangular.all('company_privileges/')
	    .customGET("",{company: companyID})
	    .then(function (res) {
	      //console.log(res);
	      return res;
	    });
	  };

	  companyPrivilegeService.create = function (companyID) {
	    return Restangular.all('company_privileges/')
	    .post(companyID)
	    .then(function (res) {
	      //console.log(res);
	      return res;
	    });
	  };

	  companyPrivilegeService.update = function (privilege) {
	    return Restangular.all('company_privileges')
	    .customGET()
	    .then(function (res) {
	      var result = $q.defer();
	      //console.log(res);
	      res.can_accept_inquiry = privilege.can_accept_inquiry;
	      res.can_apply_inquiry = privilege.can_apply_inquiry;
	      res.can_post_inquiry = privilege.can_post_inquiry;
	      res.can_change_company_properties = privilege.can_change_company_properties;
	      res.can_grant_company_privileges = privilege.can_grant_company_privileges;
	      res.is_confirmed = privilege.is_confirmed;
	      //console.log(res);

	      res.customPUT(privilege).then(function (suc) {
	      	//console.log("OK");
	      	//result=suc;
	      	result.resolve(suc);
	      	return suc;
	      }, function (suc) {
	      	//console.log(suc);
	        //result=suc;
	        result.resolve(suc);
	      	return suc;
	      });
	      return result.promise;
	    });
	  };

	  return companyPrivilegeService;
	})

	.factory('InquiryService', function (Restangular, $q) {
	  var inquiryService = {};

	  inquiryService.create = function (inquiry) {
	  	if(inquiry.activeDate.length==undefined)
	  		inquiry.activeDate = inquiry.activeDate.toJSON().substring(0,10);
	  	else
	  		console.log("NOT");
	    return Restangular.all('inquiries/')
	    .post(inquiry)
	    .then(function (res) {
	      return res;
	    });
	  };

	  inquiryService.getAllInquiries = function (currentPage, filters, orderBy, reverse) {
	    return Restangular.all('inquiries/')
	    .customGET("",{page: currentPage, name: filters.name, company: filters.company, workArea: filters.workArea, area: filters.area, activeDate: filters.activeDate,
	    	orderBy: orderBy, reverseSearch: reverse})
	    .then(function (res) {
	      return res;
	    });
	  };
	  
	  inquiryService.getAllInquiriesByCompany = function (companyID) {
	    return Restangular.all('inquiries/')
	    .customGET("",{companyID: companyID})
	    .then(function (res) {
	      return res;
	    });
	  };


	  inquiryService.getDetail = function (inquiryID) {
	    return Restangular.all('inquiries/')
	    .customGET(inquiryID)
	    .then(function (res) {
	      return res;
	    });
	  };

	  inquiryService.update = function (inquiry) {

	    return Restangular.all('inquiries')
	    .customGET(inquiry.id)
	    .then(function (res) {
	      var result = $q.defer();
	      if(inquiry.activeDate.length==undefined)
		    inquiry.activeDate = inquiry.activeDate.toJSON().substring(0,10);
	      res.name = inquiry.name;
	      res.activeDate = inquiry.activeDate;
	      res.workArea = inquiry.workArea;
	      res.details = inquiry.details;
	      res.area = inquiry.area;
	      //console.log(res);

	      res.customPUT(inquiry).then(function (suc) {
	      	//console.log("OK");
	      	//result=suc;
	      	result.resolve(suc);
	      	return suc;
	      }, function (suc) {
	      	//console.log(suc);
	        //result=suc;
	        result.resolve(suc);
	      	return suc;
	      });
	      return result.promise;
	    });
	  };

	  return inquiryService;
	})

	.factory('OfferService', function (Restangular, $q) {
	  var offerService = {};

	  offerService.create = function (offer, inquiryID) {
	  	offer.inquiry = inquiryID;
	    return Restangular.all('offers/')
	    .post(offer)
	    .then(function (res) {
	      return res;
	    });
	  };

	  offerService.getAllOffers = function (inquiryID, companyID) {
	    return Restangular.all('offers/')
	    .customGET("",{inquiryID: inquiryID, companyID: companyID})
	    .then(function (res) {
	      return res;
	    });
	  };

	  offerService.getDetail = function (offerID) {
	    return Restangular.all('offers/')
	    .customGET(offerID)
	    .then(function (res) {
	      return res;
	    });
	  };

	  offerService.update = function (offer) {
	    return Restangular.all('offers')
	    .customGET(offer.id)
	    .then(function (res) {
	      var result = $q.defer();
	      //console.log(res);
	      res.description = offer.description;
	      //console.log(res);

	      res.customPUT(offer).then(function (suc) {
	      	//console.log("OK");
	      	//result=suc;
	      	result.resolve(suc);
	      	return suc;
	      }, function (suc) {
	      	//console.log(suc);
	        //result=suc;
	        result.resolve(suc);
	      	return suc;
	      });
	      return result.promise;
	    });
	  };

	  offerService.updateRate = function (offer) {
	    return Restangular.all('offers')
	    .customGET(offer.id)
	    .then(function (res) {
	      var result = $q.defer();
	      //console.log(res);
	      res.is_offerer_confirmed = offer.is_offerer_confirmed;
	      res.is_inquirier_confirmed = offer.is_inquirier_confirmed;
	      //console.log(res);

	      res.customPUT(offer).then(function (suc) {
	      	//console.log("OK");
	      	//result=suc;
	      	result.resolve(suc);
	      	return suc;
	      }, function (suc) {
	      	//console.log(suc);
	        //result=suc;
	        result.resolve(suc);
	      	return suc;
	      });
	      return result.promise;
	    });
	  };

	  return offerService;
	})

	.factory('MeService', function (Restangular, Session) {
	  var meService = {};
	  meService.createCompanyRelation = function (user_companyID) {
	    return Restangular.all('me/')
	    .post({'usercompanyID': user_companyID})
	    .then(function (res) {
	      console.log(res);
	      return res;
	    });
	  };
	  meService.getCompanies = function (companyID) {
	    return Restangular.all('me/')
	    .customGET("",{list: 'list'})
	    .then(function (res) {
	      return res;
	    });
	  };
	  

	  return meService;
	})
	

	.service('Session', function () {
	  //this.create = function (sessionId, userId, userRole) {
	  this.create = function (session) {
	    //this.id = sessionId;
	    this.username = session.username;
	    this.user_id = session.user_id;
	    this.userRole = session.group;
	    if(session.company){
	    	this.company = session.company;
	    	this.companyID = session.companyID;
	    	this.can_accept_inquiry = session.can_accept_inquiry;
	      	this.can_apply_inquiry = session.can_apply_inquiry;
	      	this.can_post_inquiry = session.can_post_inquiry;
	      	this.can_change_company_properties = session.can_change_company_properties;
	      	this.can_grant_company_privileges = session.can_grant_company_privileges;
	    }else{
	    	this.company = "";
	    	this.companyID = "";
	    	this.can_accept_inquiry = "";
	      	this.can_apply_inquiry = "";
	      	this.can_post_inquiry = "";
	      	this.can_change_company_properties = "";
	      	this.can_grant_company_privileges = "";
	    }
	  };
	  this.destroy = function () {
	    //this.id = null;
	    this.username = null;
	    this.userRole = null;
	  };
	  return this;
	});