module.exports = function(grunt) {
	grunt.loadNpmTasks('../node_modules/grunt-angular-gettext');
	grunt.initConfig({
	  nggettext_extract: {
	    pot: {
	      files: {
					'app/po/template.pot': ['app/partials/**/*.html','app/index.html']	
				}
			},
		},
	  nggettext_compile: {
	  	all: {
	  		files: {
	  			'app/po/translations.js': ['app/po/*.po']
	  		}
	  	}
	  }
	})

};